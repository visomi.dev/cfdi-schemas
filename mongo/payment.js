const payment = {
  receiverAccount: {
    type: String,
  },
  stringPaymentType: {
    type: String,
  },
  operationNumber: {
    type: String,
  },
  amount: {
    type: Number,
  },
  receiverRfc: {
    type: String,
  },
  issuingAccount: {
    type: String,
  },
  issuingRfc: {
    type: String,
  },
  exchangeRate: {
    type: String,
  },
  bankName: {
    type: String,
  },
  certificate: {
    type: String,
  },
  wayPay: {
    type: String,
  },
  currency: {
    type: String,
  },
  string: {
    type: String,
  },
  seal: {
    type: String,
  },
};

module.exports = payment;
