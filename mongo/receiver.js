const receiver = {
  receiverFiscalResidence: {
    type: String,
    default: undefined,
  },
  receiverNumRegIdTrib: {
    type: String,
    default: undefined,
  },
  receiverCfdiUsage: {
    type: String,
  },
  receiverName: {
    type: String,
  },
  receiverRfc: {
    type: String,
  },
};

module.exports = receiver;
