const voucherSchema = require('./voucher');
const emitterSchema = require('./emitter');
const receiverSchema = require('./receiver');

const taxesSchema = require('./taxes');
const totalTaxesSchema = require('./totalTaxes');

const complementSchema = require('./complement');

const conceptSchema = require('./concept');

const cfdiSchema = {
  ...voucherSchema,

  ...emitterSchema,

  ...receiverSchema,

  ...totalTaxesSchema,

  concepts: [
    conceptSchema,
  ],

  taxes: {
    type: taxesSchema,
    default: undefined,
  },

  complement: {
    type: complementSchema,
    default: undefined,
  },

  cfdisRelated: {
    type: Array,
    default: undefined,
  },
};

module.exports = cfdiSchema;
