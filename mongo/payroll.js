const payrollSchema = {
  payrollVersion: {
    type: String,
    default: undefined,
  },
  payrollType: {
    type: String,
    default: undefined,
  },
  payrollPaymentDate: {
    type: String,
    default: undefined,
  },
  payrollPaymentPeriodStart: {
    type: String,
    default: undefined,
  },
  payrollPaymentPeriodEnd: {
    type: String,
    default: undefined,
  },
  payrollEarnedDays: {
    type: String,
    default: undefined,
  },
  payrollTotalPercepts: {
    type: String,
    default: undefined,
  },
  payrollTotalDeductions: {
    type: String,
    default: undefined,
  },
  payrollTotalOtherPayments: {
    type: String,
    default: undefined,
  },
  emitterCurp: {
    type: String,
    default: undefined,
  },
  emitterEmployerRegistration: {
    type: String,
    default: undefined,
  },
  emitterOriginEmployerRfc: {
    type: String,
    default: undefined,
  },
  receiverCurp: {
    type: String,
    default: undefined,
  },
  receiverSocialSecurityNumber: {
    type: String,
    default: undefined,
  },
  receiverJobRelationshipStart: {
    type: String,
    default: undefined,
  },
  receiverSeniority: {
    type: String,
    default: undefined,
  },
  receiverContractType: {
    type: String,
    default: undefined,
  },
  receiverSyndicalized: {
    type: String,
    default: undefined,
  },
  receiverWorkingDayType: {
    type: String,
    default: undefined,
  },
  receiverRegimeType: {
    type: String,
    default: undefined,
  },
  receiverEmployedNumber: {
    type: String,
    default: undefined,
  },
  receiverDepartment: {
    type: String,
    default: undefined,
  },
  receiverPosition: {
    type: String,
    default: undefined,
  },
  receiverHazardPosition: {
    type: String,
    default: undefined,
  },
  receiverPaymentPeriodicity: {
    type: String,
    default: undefined,
  },
  receiverBank: {
    type: String,
    default: undefined,
  },
  receiverBankAccount: {
    type: String,
    default: undefined,
  },
  receiverBaseSalary: {
    type: String,
    default: undefined,
  },
  receiverDailyBaseSalary: {
    type: String,
    default: undefined,
  },
  receiverLocalityKey: {
    type: String,
    default: undefined,
  },
  perceptsTotalEarnings: {
    type: String,
    default: undefined,
  },
  perceptsTotalCompensation: {
    type: String,
    default: undefined,
  },
  perceptsTotalRetirementBenefit: {
    type: String,
    default: undefined,
  },
  perceptsTotalTaxed: {
    type: String,
    default: undefined,
  },
  perceptsTotalExempt: {
    type: String,
    default: undefined,
  },
  deductionsTotalOtherDeductions: {
    type: String,
    default: undefined,
  },
  deductionsTotalTaxWithheld: {
    type: String,
    default: undefined,
  },
  percepts: {
    type: Array,
    default: undefined,
  },
  deductions: {
    type: Array,
    default: undefined,
  },
  otherPayments: {
    type: Array,
    default: undefined,
  },
  paidLeaves: {
    type: Array,
    default: undefined,
  },
};

module.exports = payrollSchema;
