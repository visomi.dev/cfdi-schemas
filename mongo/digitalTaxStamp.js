const digitalTaxStampSchema = {
  satCertificateNumber: {
    type: String,
  },
  rfcProvCertif: {
    type: String,
  },
  satSeal: {
    type: String,
  },
  legend: {
    type: String,
  },
  cfdSeal: {
    type: String,
  },
  uuid: {
    type: String,
    unique: true,
  },
  version: {
    type: String,
  },
  datetimeStamp: {
    type: Date,
  },
};

module.exports = digitalTaxStampSchema;
