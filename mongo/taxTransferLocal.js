const taxTransferLocal = {
  fee: {
    type: Number,
  },
  tax: {
    type: Number,
  },
  amount: {
    type: Number,
  },
};

module.exports = taxTransferLocal;
