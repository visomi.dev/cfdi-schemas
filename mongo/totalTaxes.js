const totalTaxes = {
  totalTaxesWithheld: {
    type: Number,
  },
  totalTaxesTransferred: {
    type: Number,
  },
};

module.exports = totalTaxes;
