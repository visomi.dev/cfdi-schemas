const taxRetained = {
  factorType: {
    type: String,
  },
  fee: {
    type: Number,
  },
  tax: {
    type: Number,
  },
  base: {
    type: Number,
  },
  amount: {
    type: Number,
  },
};

module.exports = taxRetained;
