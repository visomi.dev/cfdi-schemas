const voucherSchema = {
  voucherCertificateNumber: {
    type: String,
  },
  voucherExpeditionPlace: {
    type: String,
  },
  voucherExchangeRate: {
    type: String,
  },
  voucherType: {
    type: String,
  },
  voucherDiscount: {
    type: String,
  },
  voucherConsecutive: {
    type: String,
  },
  voucherCurrency: {
    type: String,
  },
  voucherSeries: {
    type: String,
  },
  voucherPaymentType: {
    type: String,
  },
  voucherPaymentMethod: {
    type: String,
  },
  voucherCertificate: {
    type: String,
  },
  voucherConfirmation: {
    type: String,
  },
  voucherVersion: {
    type: String,
  },
  voucherSeal: {
    type: String,
  },
  voucherTotal: {
    type: Number,
  },
  voucherSubTotal: {
    type: Number,
  },
  voucherDate: {
    type: Date,
  },
};

module.exports = voucherSchema;
