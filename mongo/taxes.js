const taxesSchema = {
  transfers: {
    type: Array,
    default: undefined,
  },
  retained: {
    type: Array,
    default: undefined,
  },
};

module.exports = taxesSchema;
