const taxTransfer = require('./taxTransfer');
const taxRetained = require('./taxRetained');
const taxTransferLocal = require('./taxTransferLocal');

const concept = {
  productServiceKey: {
    type: String,
  },
  identificationNumber: {
    type: String,
  },
  description: {
    type: String,
  },
  unitKey: {
    type: String,
  },
  unit: {
    type: Number,
  },
  quantity: {
    type: Number,
  },
  amount: {
    type: Number,
  },
  taxTransfers: {
    type: [
      taxTransfer,
    ],
    default: undefined,
  },
  retainedTaxes: {
    type: [
      taxRetained,
    ],
    default: undefined,
  },
  localTaxTransfers: {
    type: [
      taxTransferLocal,
    ],
    default: undefined,
  },
};

module.exports = concept;
