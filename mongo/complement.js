const payrollSchema = require('./payroll');
const paymentSchema = require('./payment');
const digitalTaxStampSchema = require('./digitalTaxStamp');

const complementSchema = {
  digitalTaxStamp: {
    type: digitalTaxStampSchema,
    default: undefined,
  },
  payments: {
    type: [
      paymentSchema,
    ],
    default: undefined,
  },
  payroll: {
    type: payrollSchema,
    default: undefined,
  },

  localTaxes: {
    type: Object,
    default: undefined,
  },
};

module.exports = complementSchema;
