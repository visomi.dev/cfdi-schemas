const emitterSchema = {
  emitterRegime: {
    type: String,
  },
  emitterName: {
    type: String,
  },
  emitterRfc: {
    type: String,
  },
};

module.exports = emitterSchema;
